mkdir -p "$HOME/.local/bin"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo "python3 \"$DIR/ubuild_main.py\" \"\$@\"" > "$HOME/.local/bin/ubuild"
chmod +x "$HOME/.local/bin/ubuild"