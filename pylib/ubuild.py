#!/usr/bin/env python3
# universal build system wrapper
# just drop in your project and hope for the best

# supported systems:
#   cmake
#   gradle


import sys

assert sys.version_info >= (3,5)

import subprocess
from subprocess import Popen, PIPE, STDOUT
import shutil
import os
import os.path
from os.path import join
import time
import re
import ctypes
import threading
from threading import Lock, Thread, Condition
from functools import lru_cache

@lru_cache(maxsize=2048)
def path_exists(path):
    return os.path.exists(path)

# see https://bugs.python.org/issue30075
if os.name == "nt":
    import msvcrt
    from ctypes import wintypes

    kernel32 = ctypes.WinDLL('kernel32', use_last_error=True)

    ERROR_INVALID_PARAMETER = 0x0057
    ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004

    def _check_bool(result, func, args):
        if not result:
            raise ctypes.WinError(ctypes.get_last_error())
        return args

    LPDWORD = ctypes.POINTER(wintypes.DWORD)
    kernel32.GetConsoleMode.errcheck = _check_bool
    kernel32.GetConsoleMode.argtypes = (wintypes.HANDLE, LPDWORD)
    kernel32.SetConsoleMode.errcheck = _check_bool
    kernel32.SetConsoleMode.argtypes = (wintypes.HANDLE, wintypes.DWORD)

    def set_conout_mode(new_mode, mask=0xffffffff):
        # don't assume StandardOutput is a console.
        # open CONOUT$ instead
        fdout = os.open('CONOUT$', os.O_RDWR)
        try:
            hout = msvcrt.get_osfhandle(fdout)
            old_mode = wintypes.DWORD()
            kernel32.GetConsoleMode(hout, ctypes.byref(old_mode))
            mode = (new_mode & mask) | (old_mode.value & ~mask)
            kernel32.SetConsoleMode(hout, mode)
            return old_mode.value
        finally:
            os.close(fdout)

    def enable_vt_mode():
        mode = mask = ENABLE_VIRTUAL_TERMINAL_PROCESSING
        try:
            return set_conout_mode(mode, mask)
        except WindowsError as e:
            if e.winerror == ERROR_INVALID_PARAMETER:
                raise NotImplementedError
            raise
else:
    # on other systems nothing to do here
    def enable_vt_mode():
        pass

try:
    # only on linux
    import resource
    resource.setrlimit(resource.RLIMIT_AS, (1024*1024*1024*4, 1024*1024*1024*5))
except:
    pass

def parse_bool(val):
    if not isinstance(val, str):
        return not not val
    if val == "0":
        return False
    val = val.lower()
    if val == "true" or val == "yes" or val == "y":
        return True
    if val == "false" or val == "no" or val == "n":
        return False
    if val == "1":
        return True
    return False

def find_up(start_dir, name):
    while start_dir:
        if path_exists(os.path.join(start_dir, name)):
            return start_dir
        if len(start_dir.split("/")) <= 1 or len(start_dir.split("\\")) == 1:
            break
        start_dir = os.path.dirname(start_dir)
    return None

class Point:
    def __init__(self, x:float = 0, y:float = 0):
        self.x = x
        self.y = y

class Graph:
    def __init__(self):
        self.points = []

    def add_point(self, point: Point):
        self.points.append(point)
    def add(self, x, y = None):
        if isinstance(x, Point):
            return self.add_point(point)
        return self.add_point(Point(x, y))
    def speed(self):
        if len(self.points) <= 1:
            return 0
        rise = self.points[len(self.points)-1].y - self.points[0].y
        run = self.points[len(self.points)-1].x - self.points[0].x
        return rise / run
class ProgressGraph(Graph):
    def __init__(self, total:float = 1.0):
        Graph.__init__(self)
        self.total = total

    @property
    def progress(self):
        if len(self.points) == 0:
            return 0.0
        return self.points[len[self.points]-1].y / self.total
    @property
    def eta(self):
        if len(self.points) <= 2:
            return 0
        speed = (self.points[-1].y - self.points[0].y) / (self.points[-1].x - self.points[0].x)
        if speed == 0:
            return 0
        done_estimate = speed*(time.monotonic() - self.points[-1].x) + self.points[-1].y
        remaining = self.total - done_estimate
        eta = remaining/speed
        return eta

    @property
    def complete(self):
        if len(self.points) == 0:
            return 0
        return self.points[len(self.points)-1].y

    @complete.setter
    def complete(self, value):
        if(value == self.complete):
            return self.complete
        x = time.monotonic() + 0.0
        value += 0.0
        if value < self.complete:
            self.points = []
        self.add(x, value)
        return self.complete

class AsyncReader:
    def __init__(self, input_stream):
        self.input_stream = input_stream
        self.lines = []
        self.condition = Condition()
        Thread(target=self.reader_thread).start()

    def reader_thread(self):
        try:
            while self.input_stream:
                line = self.input_stream.readline()
                if len(line) == 0:
                    break
                with self.condition:
                    self.lines.append(line)
                    self.condition.notify_all()
        finally:
            with self.condition:
                self.input_stream = None
                self.condition.notify_all()
    def next(self):
        with self.condition:
            while len(self.lines) == 0 and self.input_stream:
                self.condition.wait()
            if len(self.lines) > 0:
                return self.lines.pop(0)
        return None
    def wait_for_next(self):
        with self.condition:
            while len(self.lines) == 0 and self.input_stream:
                self.condition.wait()
            return len(self.lines) > 0
    
    def poll(self):
        with self.condition:
            if len(self.lines) > 0:
                return self.lines.pop(0)
        return None
        
    def __iter__(self):
        return self
    
    def __next__(self):
        next = self.next()
        if next is None:
            raise StopIteration()
        return next
    def __bool__(self):
        with self.condition:
            return len(self.lines) > 0 or self.input_stream is not None
def parse_progress(line):
    match = re.search("(\d+)\s*\/\s*(\d+)", line)
    if match:
        return [int(match[1]), int(match[2])]
    return None
def left_pad(string, length, what=' '):
    needed = length - len(string)
    if needed > 0:
        return what*needed + string
    return string

def render_progress(progress, width):
    if isinstance(progress, list):
        progress = progress[0] / progress[1]
    line = "["
    total_spots = width -2
    spots = int(total_spots*progress)
    sub = "|-="
    sub_spot = int((total_spots*progress - spots)*len(sub))
    if sub_spot != 0:
        line += "#"*int(spots) + sub[sub_spot] + " "*(total_spots - spots-1) + "]"
    else:
        line += "#"*int(spots) + " "*(total_spots - spots) + "]"

    return line

class ChangeDir:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        sys.stdout.flush()
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)
        return self

    def __exit__(self, etype, value, traceback):
        sys.stdout.flush()
        os.chdir(self.savedPath)

class FileFilter:
    def __init__(self, relative_to = None):
        if relative_to is None:
            relative_to = []
        self.relative_to = relative_to
        self.force_absolute = True
        self.force_relative = False
        pass

    def filter_path(self, path):
        path = os.path.abspath(path)
        path = path.replace("\\", "/")
        cwd = os.getcwd().replace("\\", "/")
        if self.force_absolute:
            return path
        if cwd[-1:] != "/":
            cwd = cwd + "/"
        if path[0:len(cwd)] == cwd:
            return "./" + path[len(cwd):]
        return path

    def find_file(self, file):
        max_file_len = 4096
        if os.name == "nt":
            max_file_len = 260
        # Anything too long is probably not a file
        if len(file) >= max_file_len:
            return file
        if len(file) <= 2:
            return file
        if len(file) >= 255:
            if not "/" in file:
                return file
        if file[0].isspace() or file[-1].isspace():
            return file
        for test_char in '@\"\'‘’`*?<>|[];':
            if test_char in file:
                return file

        original = file
        if path_exists(file):
            return self.filter_path(file)
        for path in self.relative_to:
            path = os.path.join(path, file)
            if path_exists(path):
                return self.filter_path(path)
        while file[0:3] == "../" or file[0:3] == "..\\":
            file = file[3:]
            if path_exists(file):
                return self.filter_path(file)
            for path in self.relative_to:
                path = os.path.join(path, file)
                if path_exists(path):
                    return self.filter_path(path)
        return original

    def process_split(self, line, sep):
        if not sep in line:
            return line
        parts = line.split(sep)
        for i in range(0, len(parts)):
            parts[i] = self.find_file(parts[i])
        return sep.join(parts)
    def process_line(self, line):
        line = self.process_split(line, "(")
        line = self.process_split(line, "\"")
        line = self.process_split(line, "'")
        line = self.process_split(line, ";")
        # windows uses ":" as drive separator so we should do this last
        line = self.process_split(line, ":")
        return line

class ArgParser:
    def __init__(self, argv):
        args = argv[1:]
        i = 0
        self.loop               = False
        self.build_type         = "release"
        self.target             = None
        self.quiet              = False
        self.project_dir        = None
        self.filter_stdin       = False
        while i < len(args):
            if args[i] == "-l" or args[i] == "--loop":
                self.loop = True
                del args[i]
                continue
            if args[i] == "-q" or args[i] == "--quiet":
                self.quiet = True
                del args[i]
                continue
            if args[i] == "-p":
                self.project_dir = os.path.abspath(args[i+1])
                if not path_exists(self.project_dir):
                    print(self.project_dir + " doesn't exist")
                    exit(1)
                del args[i]
                del args[i]
                continue

            if args[i][0] == "-" and args[i] != "-":
                print("error unknown argument " + args[i])
                exit(1)

            i += 1
        if not self.project_dir:
            self.project_dir = find_up(os.getcwd(), "ubuild_init.py")
        if len(args) == 0:
            return
        self.build_type = "release"
        if args[0] == "debug":
            self.build_type = "debug"
            del args[0]
        elif args[0] == "release":
            self.build_type = "release"
            del args[0]
        elif args[0] == "relwithdebinfo":
            self.build_type = "relwithdebinfo"
            del args[0]
        elif args[0] == "-":
            self.filter_stdin = True
            if self.project_dir == None:
                self.project_dir = os.getcwd()
        if len(args) > 0:
            self.target = args[0]

def format_path(string, variables):
    for var in variables:
        search = ''.join(["$", var, "$"])
        string = string.replace(search, variables[var])
    return string

def runtime_error(message):
    raise RuntimeError(message)

def allow_control_codes(output=None):
    if output is None:
        output = sys.stdout
    isatty = getattr(output, "isatty", None)
    if callable(isatty):
        return output.isatty()

class bcolors:
    HEADER      = '\033[95m'
    OKBLUE      = '\033[94m'
    OKGREEN     = '\033[92m'
    OK          = OKGREEN
    WARNING     = '\033[93m'
    FAIL        = '\033[91m'
    ENDC        = '\033[0m'
    BOLD        = '\033[1m'
    UNDERLINE   = '\033[4m'
    NUMBER      = '\033[94m'
    CLEAR_LINE  = '\033[2K'
def is_varchar(ch):
    return ch.isalnum() or ch == "_"

def tokenize(line):
    line = list(line)
    tokens = []
    i = 0

    token_start = None
    while i < len(line):
        if is_varchar(line[i]):
            if token_start is None:
                token_start = i
        else:
            if token_start is not None:
                tokens.append("".join(line[token_start:i]))
                token_start = None
            tokens.append(line[i])
        i += 1
    if token_start is not None:
        tokens.append("".join(line[token_start:]))
    return tokens
def color_line(line):
    tokens = tokenize(line)
    i = 0
    error_words = ["error", "ERROR", "FAILED", "failed"]
    warning_words = ["note", "warning", "WARNING"]
    ok_words = ["ok", "building", "linking", "generating", "done"]
    keywords = ["if", "while", "do", "bool", "double", "int", "float", "goto",
        "then", "from"]
    while i < len(tokens):
        lower = tokens[i].lower()
        if lower in error_words:
            tokens[i] = bcolors.FAIL + tokens[i] + bcolors.ENDC
        elif lower in warning_words:
            tokens[i] = bcolors.WARNING + tokens[i] + bcolors.ENDC
        elif re.match("^[0-9]+$", tokens[i]) or re.match("^0x[a-fA-F0-9]+$", tokens[i]):
            tokens[i] = bcolors.NUMBER + tokens[i] + bcolors.ENDC
        elif re.match("^[0-9][0-9_]+$", tokens[i]):
            tokens[i] = bcolors.NUMBER + tokens[i] + bcolors.ENDC
        elif lower in ok_words:
            tokens[i] = bcolors.OK + tokens[i] + bcolors.ENDC
        elif lower in keywords:
            tokens[i] = bcolors.NUMBER + tokens[i] + bcolors.ENDC
        i += 1

    return "".join(tokens)

class CommandError(RuntimeError):
    pass

def process_pipe(input, output_list, quiet=False):
    if output_list is None:
        output_list = []
    if not isinstance(output_list, list):
        output_list = [output_list]

    output_filter = process_pipe.output_filter
    progress = None
    progress_graph = None
    reader = AsyncReader(input)
    while reader:
        reader.wait_for_next()
        while reader:
            line = reader.poll()
            if line is None:
                break
            line = output_filter.process_line(line)
            if allow_control_codes():
                progress_next = parse_progress(line)
                if progress_next:
                    if not progress_graph:
                        progress_graph = ProgressGraph()
                    progress = progress_next
                    progress_graph.complete = progress[0]/progress[1]

            for output in output_list:
                output.write(line)

            if allow_control_codes():
                if "\n" in line:
                    # some commands disable this so we must always call to make sure
                    # coloring is enabled
                    enable_vt_mode()
                line = color_line(line)
            if not quiet:
                print(bcolors.CLEAR_LINE, end='')
                print(line, end='')
        if progress:
            line = render_progress(progress, 20)
            percent = int((progress[0]/progress[1])*100)
            line = left_pad(str(percent), 3) + "% " + line + " "
            line += str(int(progress_graph.eta)) + " eta"
            print(line + "\r", end='')
        sys.stdout.flush()

process_pipe.output_filter = FileFilter()

def system(command, output_files = None, use_filter = True, quiet=False):
    sys.stdout.flush()

    if use_filter:
        pipe = None
        try:
            pipe = Popen(command, stdout=PIPE, stderr=STDOUT, universal_newlines=True)
            process_pipe(pipe.stdout, output_files, quiet=quiet)
            pipe.wait()
            result = pipe.returncode
            pipe = None
        finally:
            if pipe:
                pipe.terminate()
                pipe.wait()
    else:
        result = subprocess.call(command)
    sys.stdout.flush()
    if result != 0:
        raise CommandError("command failed: " + repr(command))
        exit(1)

class Build:
    def __init__(self):
        self.quiet = False
        self.build_type = "Release"
        # some builds the build directory output we can't control so we don't
        # add it to base class

    def configure(self):
        pass


class CMakeBuild(Build):
    def __init__(self, project_dir, build_dir=None):
        Build.__init__(self)
        self.project_dir = project_dir
        self.build_dir = build_dir
        if shutil.which("ninja"):
            self.generator = "Ninja"
        else:
            self.generator = "Unix Makefiles"

        self.cmake = shutil.which("cmake")
        if not self.cmake:
            raise RuntimeError("no cmake. Maybe you forgot to install it")

        if not self.build_dir:
            self.build_dir = os.path.join(self.project_dir, "build/$build_type$")

    def build_type_name(self):
        self.build_type = self.build_type.lower()
        build_type = self.build_type
        if build_type == "release":
            return "Release"
        if build_type == "debug":
            return "Debug"
        if build_type == "relwithdebinfo":
            return "RelWithDebInfo"
        return "Release"

    def build_dir_expanded(self):
        return format_path(self.build_dir, {
            "build_type": self.build_type,
            "generator": self.generator.lower()
        })
    def configure(self):
        cmake_cache_file = os.path.join(self.build_dir_expanded(), "CMakeCache.txt")
        first_time = not path_exists(cmake_cache_file)
        build_dir = self.build_dir_expanded()
        if not path_exists(build_dir):
            os.makedirs(build_dir)

        with ChangeDir(self.build_dir_expanded()):
            command = [self.cmake, "-G" + self.generator, self.project_dir,
                "-DCMAKE_BUILD_TYPE=" + self.build_type_name(),
                "-DCMAKE_EXPORT_COMPILE_COMMANDS=YES"]
            try:
                system(command)
            except CommandError:
                if path_exists(cmake_cache_file):
                    os.remove(cmake_cache_file)
                raise

    def needs_configure(self):
        return not path_exists(os.path.join(self.build_dir_expanded(), "CMakeCache.txt"))
    def build(self, target=None):
        if self.needs_configure():
            self.configure()

        # for test make sure it's built
        if target == "test":
            self.build()

        command = [self.cmake, "--build", self.build_dir_expanded()]

        with open(os.path.join(self.build_dir_expanded(), "build.log"), "w") as handle:
            if target == "test":
                system(command, output_files=handle, quiet=self.quiet)

            if target:
                command.extend(["--target", target])
            system(command, output_files=handle, quiet=self.quiet)

class GradleBuild:
    def __init__(self, project_dir):
        self.project_dir = project_dir

    def configure(self):
        pass

    def build(self, target=None):
        command = ["./gradlew"]
        system(command)


