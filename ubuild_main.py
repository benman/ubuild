#!/usr/bin/env python3
# universal build system wrapper
# just drop in your project and hope for the best

# supported systems:
#   cmake
#   gradle


import sys

assert sys.version_info >= (3,5)

import subprocess
from subprocess import Popen, PIPE, STDOUT
import shutil
import os
import os.path
from os.path import join
import time
import re
import ctypes
import threading
from threading import Lock, Thread, Condition

cur_dir = os.path.dirname(os.path.realpath(__file__))

sys.path.append(join(cur_dir, "pylib"))

import ubuild

def main(args):
    # hack for windows so syntax coloring works
    ubuild.enable_vt_mode()

    if not args.project_dir:
        args.project_dir = find_up(os.getcwd(), "ubuild_init.py")
    if not args.project_dir:
        print("error: could not find ubuild_init.py")
        exit(1)

    sys.path.append(args.project_dir)
    with ubuild.ChangeDir(args.project_dir):
        main_run(args, args.project_dir)

def parse_bool(string):
    return string in ["true", "1", "True"]

def main_run(args, project_dir):
    ubuild.process_pipe.output_filter.force_absolute = parse_bool(os.environ.get("UBUILD_ABSOLUTE_PATHS"))
    os.environ["UBUILD_ABSOLUTE_PATHS"] = "1"

    if args.filter_stdin:
        ubuild.process_pipe(sys.stdin, output_list=None, quiet=args.quiet)
        return
    build = None
    try:
        import ubuild_init
        build = ubuild_init.build
    except NameError as ex:
        pass
    except AttributeError as ex:
        pass
    except ModuleNotFoundError:
        if not project_dir:
            raise

    if not build and os.path.exists("CMakeLists.txt"):
        build = ubuild.CMakeBuild(project_dir)

    if not build:
        print("Could not detect build system")
        exit(0)
    build.build_type = args.build_type
    build.quiet = args.quiet
    start_time = time.monotonic()
    try:
        build.build(args.target)
    except KeyboardInterrupt:
        exit(1)
    finally:
        end_time = time.monotonic()
        print("build took {} seconds".format(str(end_time - start_time)))

if __name__ == "__main__":
    args = ubuild.ArgParser(sys.argv)

    while args.loop:
        print("ubuild: start")
        try:
            main(args)
        except ubuild.CommandError:
            pass
        print("ubuild: end")
        
        try:
            time.sleep(7)
        except KeyboardInterrupt:
            exit(1)

    if not args.loop:
        try:
            main(args)
        except ubuild.CommandError:
            exit(1)